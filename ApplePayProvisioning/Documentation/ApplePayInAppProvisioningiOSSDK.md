# Apple Pay In-App Provisioning iOS SDK

## Getting started
The following documentation demonstrates how to integrate the Apple Pay In-App Provisioning iOS SDK into an existing iOS app.

## Requirements
### Version 5.0.0 or later
* iOS 14.0 or later
* Xcode 11.0+
### Version 3.0.0 or later
* iOS 13.0 or later
* Xcode 11.0+
### Version 2.0.0 or earlier
* iOS 11.0 or later
* Xcode 11.0+

## Usage

### Installation
This framework is meant to be installed via  [cocoapods](https://cocoapods.org/)

#### Podfile
Update `Podfile`:
-   Add `source` options for _ApplePayProvisioning_ repository
-   Add `ApplePayProvisioning` dependency to your target

```ruby
# If ApplePayProvisioning version 2.0.0 or earlier is used, change to platform :ios, '11.0'
platform :ios, '14.0'

source 'https://github.com/CocoaPods/Specs.git'
source 'https://gitlab.com/phyreapp/ios-specs.git'

target 'YourApp' do
  use_frameworks!

  pod 'ApplePayProvisioning', '3.0.0'

end

post_install do |installer|
    installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
            config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'

        end
    end
end
```
Then run this in the project directory
```ruby
pod install
```
### Import
Import the framework in any file you intend to use it as follows

```swift
import ApplePayProvisioning
```

### Initialization
**There is no need for any additional setup or initialization.**
The shared instance of AddCardToApplePayManager exposes a generic interface to check if a card can be added to ApplePay and the option to add it. The instance can be accessed via

```swift
AddCardToApplePayManager.shared
```

### Card status check

Use the following function to check whether a card can be added to ApplePay. It takes as arguments a string representing the last four digits of the payment card number and a device type. This is the type of the device to witch the card would be added.
The `DeviceType` enumeration suports:
* `phone`
* `watch`
* `all` - both `phone` and `watch`

```swift
public func canAddCardToApplePay(panLastFour: String, onDeviceType: DeviceType  = .all) -> AddCardToApplePayStatus
```

 The result of the check is represented via the `AddCardToApplePayStatus` enumeration. 
 
```swift
enum AddCardToApplePayStatus {
  case unavailable
  case alreadyAdded
  case notAdded
}
```

* `unavailable` is returned when the app cannot add any card to Apple Pay.
* `alreadyAdded` is returned when the card in consideration is already in Apple Pay and cannot be added again
* `notAdded` is returned when the card in consideration can be added to Apple Pay.

#### Example
```swift
func checkCanAddCard(with panLastFour: String) {
    //Ask the SDK for the current status
       let canAddCardStatus = AddCardToApplePayManager.shared.canAddCardToApplePay(panLastFour: panLastFour)
    
    switch canAddCardStatus {
    case .unavailable:
        //Handle status
       print("The option to add any card from this app to Apple Pay is unavailable.")
            
   case .alreadyAdded:
        //Handle status
        print("The card is already added to Apple Pay and cannot be done again.")
        
    case .notAdded:
        //Handle status
        print("The card is not previously added and the option to do so is available.")
    }
}
```

### Add card to ApplePay
Use the following function when you want to add a card to ApplePay. It presents an interface so a presenting view controller must be specified. You should pass the client id that is specifically generated for you and an authentication id or token along with the other card related information - card Id, the last four digits of the card number, cardholder names and description.

When the process is finished a `completionHandler` is called with either `success` or `failure` as a result. Any additional work related with the completion of adding the card (e.g. showing a success message or displaying an error) should be done within this completion handler.

```swift
public func addCardToApplePay(fromViewController: UIViewController, clientId: String, authenticationId: String, panLastFour: String, clientPaymentCardId: String, cardholderNames: String, cardDescription: String, completionHandler: @escaping (Result<(), Error>) -> Void) 
```

#### Example
```swift
   let presentingViewController = UIViewController() //Your view controller
   let clientId = "5d4d6e4c-522e-4f42-b0e2-d239ebdef9de"
   let authenticationId = "Your authentication id or token"
   let paymentCardId = "Card Id"
   let panLastFour = "0000"
   let cardholderNames = "Cardholder names"
   let cardDescription = "Short description"
        
   AddCardToApplePayManager.shared.addCardToApplePay(from: presentingViewController, clientId: clientId, authenticationId: authenticationId, panLastFour: panLastFour, clientPaymentCardId: paymentCardId, cardholderNames: cardholderNames, cardDescription: cardDescription) { result in
        switch result {
        case .success:
            //Do something when card is successfully added to ApplePay
            print("Success")
    
        case .failure(let error):
            //Handle and show error
            print("Error adding to Apple Pay: \(error.localizedDescription)")
        }
    }
```
#### Errors
The following listed errors can be returned from the SDK in case the process was unsuccessful

* PKAddPaymentPassError.unsupported

Returned when the app cannot add cards to Apple Pay.

* PKAddPaymentPassError.userCancelled

Returned when the user cancels the request to add a card to Apple Pay

* PKAddPaymentPassError.systemCancelled

Returned when the system cancels the request to add a card to Apple Pay.
* ProvisioningErrorCodes.invalidClientID

Returned when clientId is invalid or wrong.

* ProvisioningErrorCodes.requestValidationError

Returned when request input fields are invalid. More details about this error are going to be logged in console.

* ProvisioningErrorCodes.provisioningNotAuthorized

Returned when client don't authorize request for the provisioning.

* ProvisioningErrorCodes.blockedUser

Returned when user is blocked.

* ProvisioningErrorCodes.invalidCardToken

Returned when user card token is invalid.

* ProvisioningErrorCodes.expiredCard

Returned when user card is expired.

* ProvisioningErrorCodes.internalServerError

Returned when there is unexpected or unknown error on the server side with status code 500.

#### Debuging
For additional debug details use
```swift
AddCardToApplePayManager.shared.debugPrintEnabled = true
```

### Create  pass entry for every card available for adding to Apple Wallet

Use the following method to create a pass entry for card that will be provisioned. The method aceepts the following arguments:
* cardholderNames - String representing the names of the cardholder
*   panLastFour - String representing the last four digits of the payment card number
* cardDescription - A meaningful description of the card. This description is visible in the Apple wallet when the provisioning process is started
* cardToken - String that uniquely identifies the card
* cardImage - CGImage representing the unique card artwork shown when the provisioning process is started
```swift
public  func  makePassEntryForCardWith(cardholderNames: String, panLastFour: String, cardDescription: String, cardToken: String, cardImage: CGImage)  ->  PKIssuerProvisioningExtensionPassEntry?
```
# Adding Wallet Extensions

Wallet Extensions feature is app extension based and lets the issuer app extend custom functionality and content, and make it available in Apple Wallet. All of the methods are triggered by Apple Wallet and the app extensions ability to enable behavior is solely based on the completion handlers and return values. This feature relies on two extensions:

1. **Issuer app should provide a non-UI extension** (subclass of `PKIssuerProvisioningExtensionHandler`) to report on the status of the extension and the payment passes available for provisioning like when adding payment passes to Apple Pay from within the issuer app.

2. **Issuer app should provide a UI extension** (`UIViewController` that conforms to `PKIssuerProvisioningExtensionAuthorizationProviding`) to perform authentication of the user if the non-UI extension reports in its status that authentication is required. The UI extension is a separate screen that uses the same issuer app login credentials. UI extension is not a redirect to the issuer app.

Wallet Extensions do not use a specific extension type template.

## Creating App Extensions

Select Intents Extension as the base. Create two app extensions IssuerNonUIExtension and IssuerUIExtension following the steps below.

- Add an app extension to Xcode app project, choose `File` > `New` > `Target`, select `iOS` > `Application Extension` > `Intents Extension`.

![Issuer Extensions Target](create_intents_extension.png "Issuer Extensions Target")

- Set options for the new target. Create a unique bundle ID for the extension and add App IDs to associatedApplicationIdentifiers in PNO metadata.

![Wallet Extension](create_wallet_extension_target.png "Wallet Extension")

- Check the box for "Include UI Extension" if UI Extension will be needed.

For example,

|Bundle Identifier|
|----|
|ABCDE12345.com.phyrewallet.app|
|ABCDE12345.com.phyrewallet.app.WalletExtension|
|ABCDE12345.com.phyrewallet.app.WalletExtensionUI|

- Activate the created scheme, if asked.
- Add `PassKit.framework` to `Frameworks and Libraries` of the `IssuerNonUIExtension` and `IssuerUIExtension` targets, and remove `Intents.framework` which is not needed.
- Remove `Intents.framework` of the `IssuerUIExtension` target.

## App Extension Cocoapods

Add newly created target in Podfile and add 'ApplePayProvisioning' pod with version higher than 5.0.0

## App Extensions Configuration

Register both app extension bundle identifiers in Apple Developer portal Identifiers section. Create provisioning profiles accordingly.

Create a new App Group if currently there is no App Group for the app and add the main app and both app extension bundle identifiers to the app group, so data can be shared between them.

## Implementing Issuer Authorization Provider Extension

IssuerAuthorizationExtensionHandler UI extension should conform to `PKIssuerProvisioningExtensionAuthorizationProviding` protocol. Apple Wallet interrogates the issuer app to determine the user's authorization status, and the authorization UI extension performs user authentication.

App UI extension has a memory limit of 60 MB, developers are responsible to optimize the code and libraries to fit this requirement.

```swift
import PassKit
import UIKit

@available(iOS 14.0, *)
class IssuerAuthorizationExtensionHandler: UIViewController, PKIssuerProvisioningExtensionAuthorizationProviding {
    var completionHandler: ((PKIssuerProvisioningExtensionAuthorizationResult) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Set up view and authenticate user.
    }

    func authenticateUser() {
        let userAuthenticated = true // User authentication outcome.
        
        let authorizationResult: PKIssuerProvisioningExtensionAuthorizationResult = userAuthenticated ? .authorized : .canceled

        self.completionHandler?(authorizationResult)
    }
}
```
Implementing Issuer Extension Handler
IssuerExtensionHandler non-UI class must be a subclass of PKIssuerProvisioningExtensionHandler. Issuer app must be installed and the user must open the issuer app at least once for the system to call the issuer extension handler.

App Non-UI extension has a memory limit of 55 MB, developers are responsible to optimize the code and libraries to fit this requirement.

```swift
import PassKit
import ApplePayProvisioning

@available(iOS 14.0, *)
class IssuerExtensionHandler: PKIssuerProvisioningExtensionHandler {

    // Determines if there is a pass available and if adding the pass requires authentication.
    // The completion handler takes a parameter status of type PKIssuerProvisioningExtensionStatus that indicates
    // whether there are any payment cards available to add as Wallet passes.

    // PKIssuerProvisioningExtensionStatus has the following properties:
    // requiresAuthentication: Bool - authorization required before passes can be added.
    // passEntriesAvailable: Bool - passes will be available to add (at least one).
    // remotePassEntriesAvailable: Bool - passes will be available to add on the remote device (at least one).

    // The handler should be invoked within 100ms. The extension is not displayed to the user in Wallet if this criteria is not met.
    override func status(completion: @escaping (PKIssuerProvisioningExtensionStatus) -> Void) {
        let status = PKIssuerProvisioningExtensionStatus()
        
        // For this example let's say that we store user available cards pans in shared UserDefaults (UserData.cardsPan)

        status.passEntriesAvailable = UserData.cardsPan.first { pan in
            // call SDK to check pan for phone pass
            return AddCardToApplePayManager.shared.canAddCardToApplePay(panLastFour: pan, onDeviceType: .phone) == .notAdded
        } != nil

        status.remotePassEntriesAvailable = UserData.cardsPan.first { pan in
            // call SDK to check pan for watch pass
            return AddCardToApplePayManager.shared.canAddCardToApplePay(panLastFour: pan, onDeviceType: .watch) == .notAdded
        } != nil
        
        completion(status)
    }

    // Finds the list of passes available to add to an iPhone.
    // The completion handler takes a parameter entries of type Array<PKIssuerProvisioningExtensionPassEntry> representing
    // the passes that are available to add to Wallet.

    // PKIssuerProvisioningExtensionPaymentPassEntry has the following properties:
    // art: CGImage - image representing the card displayed to the user. The image must have square corners and should not include personally identifiable information like user name or account number.
    // title: String - a name for the pass that the system displays to the user when they add or select the card.
    // identifier: String - an internal value the issuer uses to identify the card. This identifier must be stable.
    // addRequestConfiguration: PKAddPaymentPassRequestConfiguration - the configuration data used for setting up and displaying a view controller that lets the user add a payment pass.

    // Do not return payment passes that are already present in the user’s pass library.
    // The handler should be invoked within 20 seconds or will be treated as a failure and the attempt halted.
    override func passEntries(completion: @escaping ([PKIssuerProvisioningExtensionPassEntry]) -> Void) {

       // Use canAddCardToApplePay method with parameter 'onDeviceType: .phone' to determine if the card is added already to Apple Wallet for iPhone
       // Use makePassEntryForCardWith to create a Pass Entry for every card that is available for adding to Apple Wallet
    }

    // Finds the list of passes available to add to an Apple Watch.
    // The completion handler takes a parameter entries of type Array<PKIssuerProvisioningExtensionPassEntry> representing
    // the passes that are available to add to Apple Watch.

    // PKIssuerProvisioningExtensionPaymentPassEntry has the following properties:
    // art: CGImage - image representing the card displayed to the user. The image must have square corners and should not include personally identifiable information like user name or account number.
    // title: String - a name for the pass that the system displays to the user when they add or select the card.
    // identifier: String - an internal value the issuer uses to identify the card. This identifier must be stable.
    // addRequestConfiguration: PKAddPaymentPassRequestConfiguration - the configuration data used for setting up and displaying a view controller that lets the user add a payment pass.

    // Do not return payment passes that are already present in the user’s pass library.
    // The handler should be invoked within 20 seconds or will be treated as a failure and the attempt halted.
    override func remotePassEntries(completion: @escaping ([PKIssuerProvisioningExtensionPassEntry]) -> Void) {

       // Use canAddCardToApplePay method with parameter 'onDeviceType: .watch' to determine if the card is added already to Apple Wallet for Apple Watch
       // Use makePassEntryForCardWith to create a Pass Entry for every card that is available for adding to Apple Wallet 
    }

    // identifier: String - an internal value the issuer uses to identify the card.
    // configuration: PKAddPaymentPassRequestConfiguration - the configuration the system uses to add a secure pass. This configuration is prepared in methods passEntriesWithCompletion: and remotePassEntriesWithCompletion:.
    // certificates, nonce, nonceSignature - parameters are generated by Apple Pay identically to PKAddPaymentPassViewControllerDelegate methods.

    // The completion handler is called by the system for the data needed to add a card to Apple Pay.
    // This handler takes a parameter request of type PKAddPaymentPassRequestConfiguration that contains the card data the system needs to add a card to Apple Pay.

    // The continuation handler must be called within 20 seconds or an error is displayed.
    // Subsequent to timeout, the continuation handler is invalid and invocations is ignored.
    override func generateAddPaymentPassRequestForPassEntryWithIdentifier(
        _ identifier: String,
        configuration: PKAddPaymentPassRequestConfiguration,
        certificateChain certificates: [Data],
        nonce: Data,
        nonceSignature: Data,
        completionHandler completion: @escaping (PKAddPaymentPassRequest?) -> Void) {
            
            let clientId = "5d4d6e4c-522e-4f42-b0e2-d239ebdef9de"
            let clientAuthenticationId = "Your authentication id or token"
            
            AddCardToApplePayManager.shared.addPaymentPass(clientId: clientId, clientAuthenticationId: clientAuthenticationId, clientPaymentCardId: identifier, certificates: certificates, nonce: nonce, nonceSignature: nonceSignature, completionHandler: completion)
    }
}
```

## Update Info.plist

Updating Extension's Info.plist
Modify NSExtension dictionary in extension's Info.plist, delete NSExtensionAttributes entry.

NSExtensionPointIdentifier and NSExtensionPrincipalClass should be specified in the extension Info.plist properties dictionary:

Non-UI App Extension

| Key      | Type | Value |
| ----------- | ----------- |----------- |
| NSExtensionPointIdentifier    | String | com.apple.PassKit.issuer-provisioning |
| NSExtensionPrincipalClass | String | $(PRODUCT_MODULE_NAME).IssuerExtensionHandler |

UI App Extension

| Key      | Type | Value |
| ----------- | ----------- |----------- |
| NSExtensionPointIdentifier |  String |    com.apple.PassKit.issuer-provisioning.authorization |
| NSExtensionPrincipalClass |   String |    $(PRODUCT_MODULE_NAME).IssuerAuthorizationExtensionHandler |

## Code Signing Entitlements

Issuer Extensions use the same entitlement file used for issuer app In-App Provisioning.

